#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#define AUDIBLE_RANGE_START 20
#define NUM_SAMPLES 96000

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    ListModel = new QStandardItemModel(this);
    ui->playlist->setModel(ListModel);
    ListModel->setHorizontalHeaderLabels(QStringList()  << tr("Audio Track") << tr("File Path"));
    ui->playlist->hideColumn(1);
    ui->playlist->verticalHeader()->setVisible(false);
    ui->playlist->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->playlist->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->playlist->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->playlist->horizontalHeader()->setStretchLastSection(true);

    Player = new QMediaPlayer(this);
    Playlist = new QMediaPlaylist(Player);
    Player->setPlaylist(Playlist);
    Player->setVolume(70);
    Playlist->setPlaybackMode(QMediaPlaylist::Loop);
    connect(ui->add, &QPushButton::clicked, this, &MainWindow::add_clicked);
    connect(ui->previous, &QPushButton::clicked, Playlist, &QMediaPlaylist::previous);
    connect(ui->next, &QPushButton::clicked, Playlist, &QMediaPlaylist::next);
    connect(ui->play, &QPushButton::clicked, Player, &QMediaPlayer::play);
    connect(ui->pause, &QPushButton::clicked, Player, &QMediaPlayer::pause);
    connect(ui->stop, &QPushButton::clicked, Player, &QMediaPlayer::stop);

    connect(ui->playlist, &QTableView::doubleClicked, [this](const QModelIndex &index)
    {
        Playlist->setCurrentIndex(index.row());
    });
    connect(Playlist, &QMediaPlaylist::currentIndexChanged, [this](int index){
        ui->label->setText(ListModel->data(ListModel->index(index, 0)).toString());
    });

    slider = new QSlider(this);
    bar = new QProgressBar(this);
    slider->setOrientation(Qt::Horizontal);

    ui->statusBar->addPermanentWidget(slider);
    ui->statusBar->addPermanentWidget(bar);

    connect(Player, &QMediaPlayer::durationChanged, slider, &QSlider::setMaximum);
    connect(Player, &QMediaPlayer::positionChanged, slider, &QSlider::setValue);
    connect(slider, &QSlider::sliderMoved, Player, &QMediaPlayer::setPosition);

    connect(Player, &QMediaPlayer::durationChanged, bar, &QProgressBar::setMaximum);
    connect(Player, &QMediaPlayer::positionChanged, bar, &QProgressBar::setValue);

    MainWindow::makePlot();

    //new

    for (int i = 0; i < NUM_SAMPLES; i ++) {
        mIndices.append((double)i);
        mSamples.append(0);
    }

    double freqStep = (double)SAMPLE_FREQ / (double)NUM_SAMPLES;
    double f = AUDIBLE_RANGE_START;
    while (f < AUDIBLE_RANGE_END) {
        mFftIndices.append(f);
        f += freqStep;
    }

    //Set up FFT plan
    mFftIn  = fftw_alloc_real(NUM_SAMPLES);
    mFftOut = fftw_alloc_real(NUM_SAMPLES);
    mFftPlan = fftw_plan_r2r_1d(NUM_SAMPLES, mFftIn, mFftOut, FFTW_R2HC,FFTW_ESTIMATE);

    //old
    auto stateMachine = new QStateMachine(this);
    auto Locked = new QState(stateMachine);
    auto Unlocked = new QState(stateMachine);

    Locked->assignProperty(ui->add, "enabled", true);
    Locked->assignProperty(ui->next, "enabled", false);
    Locked->assignProperty(ui->play, "enabled", false);
    Locked->assignProperty(ui->stop, "enabled", false);
    Locked->assignProperty(ui->previous, "enabled", false);
    Locked->assignProperty(ui->pause, "enabled", false);
    Locked->assignProperty(ui->mute, "enabled", false);
    Locked->assignProperty(ui->volume, "enabled", false);
    Locked->assignProperty(ui->playlist, "enabled", false);

    Locked->addTransition(this, SIGNAL(added()), Unlocked);

    Unlocked->assignProperty(ui->add, "enabled", true);
    Unlocked->assignProperty(ui->next, "enabled", true);
    Unlocked->assignProperty(ui->play, "enabled", true);
    Unlocked->assignProperty(ui->stop, "enabled", true);
    Unlocked->assignProperty(ui->previous, "enabled", true);
    Unlocked->assignProperty(ui->pause, "enabled", true);
    Unlocked->assignProperty(ui->mute, "enabled", true);
    Unlocked->assignProperty(ui->volume, "enabled", true);
    Unlocked->assignProperty(ui->playlist, "enabled", true);

    stateMachine->setInitialState(Locked);
    stateMachine->start();
}

MainWindow::~MainWindow()
{
    delete ui;

    fftw_free(mFftIn);
    fftw_free(mFftOut);
    fftw_destroy_plan(mFftPlan);
}

void MainWindow::add_clicked()
{
    QStringList files=QFileDialog::getOpenFileNames(this, tr("Open files"), "/home", tr("Audio Files (*.mp3)"));
    foreach (QString filePath, files)
    {
        QList <QStandardItem*> Items;
        Items.append(new QStandardItem(QDir(filePath).dirName()));
        //QString linux_path=(QUrl::fromLocalFile(QFileInfo(filePath).absoluteFilePath())).toEncoded();
        Items.append(new QStandardItem("file://"+filePath));
        ListModel->appendRow(Items);
        if(Playlist->addMedia(QUrl("file://"+filePath)))
            emit added();
    }
}

void MainWindow::on_volume_valueChanged(int value)
{
   Player->setVolume(value);
}

void MainWindow::on_mute_clicked()
{
    if(ui->mute->text()=="Mute")
    {
        Player->setMuted(true);
        ui->mute->setText("Unmute");
    }
    else
    {
        Player->setMuted(false);
        ui->mute->setText("Mute");
    }
}

void MainWindow::makePlot()
{
    ui->customPlot1->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
    ui->customPlot1->legend->setVisible(false);
    ui->customPlot1->yAxis->setLabel("Amplitude");
    ui->customPlot1->xAxis->setLabel("Sample");
    ui->customPlot1->yAxis->setRange(-0.1, 0.1);
    ui->customPlot1->clearGraphs();
    ui->customPlot1->addGraph();

    ui->customPlot1->graph()->setPen(QPen(Qt::lightGray));
    ui->customPlot1->graph()->setName("Audio In");

    ui->customPlot1->xAxis->setBasePen(QPen(Qt::white, 1));
    ui->customPlot1->yAxis->setBasePen(QPen(Qt::white, 1));
    ui->customPlot1->xAxis->setTickPen(QPen(Qt::white, 1));
    ui->customPlot1->yAxis->setTickPen(QPen(Qt::white, 1));
    ui->customPlot1->xAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->customPlot1->yAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->customPlot1->xAxis->setTickLabelColor(Qt::white);
    ui->customPlot1->yAxis->setTickLabelColor(Qt::white);
    ui->customPlot1->xAxis->grid()->setPen(QPen(QColor(40, 40, 40), 1, Qt::DotLine));
    ui->customPlot1->yAxis->grid()->setPen(QPen(QColor(40, 40, 40), 1, Qt::DotLine));
    ui->customPlot1->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->customPlot1->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->customPlot1->xAxis->grid()->setSubGridVisible(true);
    ui->customPlot1->yAxis->grid()->setSubGridVisible(true);
    ui->customPlot1->xAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->customPlot1->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->customPlot1->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    ui->customPlot1->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    QLinearGradient plotGradient;
    plotGradient.setStart(0, 0);
    plotGradient.setFinalStop(0, 350);
    plotGradient.setColorAt(0, QColor(80, 80, 80));
    plotGradient.setColorAt(1, QColor(50, 50, 50));
    ui->customPlot1->setBackground(plotGradient);
    QLinearGradient axisRectGradient;
    axisRectGradient.setStart(0, 0);
    axisRectGradient.setFinalStop(0, 350);
    axisRectGradient.setColorAt(0, QColor(80, 80, 80));
    axisRectGradient.setColorAt(1, QColor(3, 60, 30));
    ui->customPlot1->axisRect()->setBackground(axisRectGradient);
    ui->customPlot1->rescaleAxes();

    // Setup plot 2
    ui->customPlot2->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
    ui->customPlot2->legend->setVisible(false);
    ui->customPlot2->yAxis->setLabel("");
    ui->customPlot2->xAxis->setLabel("Frequency");
    ui->customPlot2->xAxis->setRange(AUDIBLE_RANGE_START, AUDIBLE_RANGE_END);
    ui->customPlot2->yAxis->setRange(0.0, 50.0);
    ui->customPlot2->clearGraphs();
    ui->customPlot2->addGraph();

    ui->customPlot2->graph()->setPen(QPen(Qt::lightGray));
    ui->customPlot2->graph()->setName("fft");

    ui->customPlot2->xAxis->setBasePen(QPen(Qt::white, 1));
    ui->customPlot2->yAxis->setBasePen(QPen(Qt::white, 1));
    ui->customPlot2->xAxis->setTickPen(QPen(Qt::white, 1));
    ui->customPlot2->yAxis->setTickPen(QPen(Qt::white, 1));
    ui->customPlot2->xAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->customPlot2->yAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->customPlot2->xAxis->setTickLabelColor(Qt::white);
    ui->customPlot2->yAxis->setTickLabelColor(Qt::white);
    ui->customPlot2->xAxis->grid()->setPen(QPen(QColor(40, 40, 40), 1, Qt::DotLine));
    ui->customPlot2->yAxis->grid()->setPen(QPen(QColor(40, 40, 40), 1, Qt::DotLine));
    ui->customPlot2->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->customPlot2->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->customPlot2->xAxis->grid()->setSubGridVisible(true);
    ui->customPlot2->yAxis->grid()->setSubGridVisible(true);
    ui->customPlot2->xAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->customPlot2->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->customPlot2->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    ui->customPlot2->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    QLinearGradient plot2Gradient;
    plot2Gradient.setStart(0, 0);
    plot2Gradient.setFinalStop(0, 350);
    plot2Gradient.setColorAt(0, QColor(80, 80, 80));
    plot2Gradient.setColorAt(1, QColor(50, 50, 50));
    ui->customPlot2->setBackground(plot2Gradient);
    QLinearGradient axisRect2Gradient;
    axisRect2Gradient.setStart(0, 0);
    axisRect2Gradient.setFinalStop(0, 350);
    axisRect2Gradient.setColorAt(0, QColor(80, 80, 80));
    axisRect2Gradient.setColorAt(1, QColor(100, 0, 30));
    ui->customPlot2->axisRect()->setBackground(axisRect2Gradient);
    ui->customPlot2->rescaleAxes();
}

void MainWindow::processAudioIn()
{
    mInputBuffer.seek(0);
    QByteArray ba = mInputBuffer.readAll();

    int num_samples = ba.length() / 2;
    int b_pos = 0;
    for (int i = 0; i < num_samples; i ++) {
        int16_t s;
        s = ba.at(b_pos++);
        s |= ba.at(b_pos++) << 8;
        if (s != 0) {
            mSamples.append((double)s / 32768.0);
        } else {
            mSamples.append(0);
        }
    }
    mInputBuffer.buffer().clear();
    mInputBuffer.seek(0);

    samplesUpdated();
}

void MainWindow::stateChangeAudioIn(QAudio::State s)
{
    qDebug() << "State change: " << s;
}

void MainWindow::samplesUpdated()
{
    int n = mSamples.length();
    if (n > 96000) mSamples = mSamples.mid(n - NUM_SAMPLES,-1);

    memcpy(mFftIn, mSamples.data(), NUM_SAMPLES*sizeof(double));

    fftw_execute(mFftPlan);

    QVector<double> fftVec;

    for (int i = (NUM_SAMPLES/SAMPLE_FREQ)*AUDIBLE_RANGE_START;
             i < (NUM_SAMPLES/SAMPLE_FREQ)*AUDIBLE_RANGE_END;
             i ++) {
        fftVec.append(abs(mFftOut[i]));
    }

    ui->customPlot1->graph(0)->setData(mIndices,mSamples);
    ui->customPlot1->xAxis->rescale();
    ui->customPlot1->replot();

    ui->customPlot2->graph(0)->setData(mFftIndices.mid(0,fftVec.length()),fftVec);
    ui->customPlot2->replot();
}

void MainWindow::on_play_clicked()
{
    QAudioFormat format;
    format.setSampleRate(48000);
    format.setChannelCount(1);
    format.setSampleType(QAudioFormat::SignedInt);
    format.setCodec("raw");
    format.setSampleSize(16);

    if (mAudioIn) delete mAudioIn;
    mAudioIn = nullptr;

    mAudioIn = new QAudioInput(format, this);
    mAudioIn->setVolume(1.0);
    mAudioIn->setNotifyInterval(100);


    connect(mAudioIn, &QAudioInput::notify,
            this, &MainWindow::processAudioIn);
    connect(mAudioIn, &QAudioInput::stateChanged,
            this, &MainWindow::stateChangeAudioIn);

    mInputBuffer.open(QBuffer::ReadWrite);
    mAudioIn->start(&mInputBuffer);
}

void MainWindow::on_freq_clicked()
{
    SAMPLE_FREQ = QInputDialog::getInt(this, "Frequency", "SAMPLE_FREQ");
    ui->SAMPLE_FREQ->display(SAMPLE_FREQ);
}

void MainWindow::on_dial_sliderMoved(int position)
{
    AUDIBLE_RANGE_END = position;
    ui->AUDIBLE_RANGE_END->display(AUDIBLE_RANGE_END);
}
