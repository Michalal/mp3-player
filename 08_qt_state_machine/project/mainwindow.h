#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItem>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QApplication>
#include <QState>
#include <QStateMachine>
#include <QFileDialog>
#include <QProgressBar>
#include <QSlider>
#include <fftw3.h>
#include <QBuffer>
#include <QtMultimedia/QAudioInput>
#include <QAudioOutput>
#include <QInputDialog>
#include "qcustomplot.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void added();

private slots:
    void add_clicked();
    void on_volume_valueChanged(int value);
    void on_mute_clicked();
    void makePlot();

    void processAudioIn();
    void stateChangeAudioIn(QAudio::State s);
    void on_play_clicked();
    void on_freq_clicked();

    void on_dial_sliderMoved(int position);

private:
    Ui::MainWindow *ui;
    QStandardItemModel *ListModel;
    QMediaPlayer *Player;
    QMediaPlaylist *Playlist;
    QProgressBar* bar;
    QSlider* slider;

    void samplesUpdated();
    int SAMPLE_FREQ;
    int AUDIBLE_RANGE_END;

    QList<QAudioDeviceInfo> mInputDevices;
    QAudioInput *mAudioIn = nullptr;
    QBuffer  mInputBuffer;

    QVector<double> mSamples;
    QVector<double> mIndices;
    QVector<double> mFftIndices;

    fftw_plan mFftPlan;
    double *mFftIn;
    double *mFftOut;

};

#endif // MAINWINDOW_H
